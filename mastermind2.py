import sys
import random

# MASTERMIND in Python 3

class Mastermind(object):
    def __init__(self, rounds: int):
        self.colours = ["red",
                        "green",
                        "blue",
                        "yellow",
                        "black",
                        "white"]
        self.rounds = rounds
        self.correct_answer = [random.choice(self.colours) for i in range(4)]
        
    def game_round(self, turn_round: int):
        if turn_round == self.rounds:
            print("Final round!")
        else:
            print("Round {}".format(turn_round))
        player_guess = []
        while len(player_guess) < 4:
                guess = input("Colour #{}: ".format(len(player_guess) + 1))
                if guess in self.colours:
                    player_guess.append(guess)
                    continue
                print("That colour is no good!")
        if player_guess == self.correct_answer:
            print("Congratulations! You were correct!")
            return True
        print("You guessed: {}".format(player_guess))
        right_colours = 0
        right_positions = 0
        correct_answer = self.correct_answer.copy()
        for index, colour in enumerate(player_guess): # [red, blue, green, yellow]
            if colour == self.correct_answer[index]:
                right_positions += 1
            elif colour in correct_answer:
                correct_answer.remove(colour)
                right_colours += 1
        print("Correct colours:{}\nCorrect positions: {}".format(right_colours,right_positions))
        return False
        
    def run(self):
        game_won = False
        current_round = 1
        while not game_won:
            if self.game_round(current_round):
                break
            if current_round >= self.rounds:
                print("GAME OVER! You ran out of guesses.")
                print("The correct answer was:\n{}".format(self.correct_answer))
                break
            current_round += 1

###############################################

def main():
    while True:
        difficulty_levels = {"easy": 10,
                             "medium":8,
                             "hard": 6,
                             "insane": 4}
        difficulty = ""
        while not difficulty:
            try:
                difficulty = difficulty_levels[input("Choose your difficulty: ")]
            except KeyError:
                print("That is not a difficulty level. Choose easy, medium, hard or insane.")
        game = Mastermind(difficulty)
        game.run()
        if input("Play again? ").strip():
            continue
        break
    print("Thanks for playing!")
    
if __name__ == "__main__":
    main()
